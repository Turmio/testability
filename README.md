[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/Turmio/testability)

# Testability demo repository

This repository is for demoing purposes only. Based on https://developer.okta.com/blog/2018/11/20/build-crud-spring-and-vue

## Developer environment

Developer enviroment is provided via https://gitpod.io/