package fi.soisenniemi.testability;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TestabilityApplicationTests {


    @Autowired
    private TodoRepository todoRepository;

	@Test
	void contextLoads() {
    }
    
    @Test
    void saveToDatabase() {
        Todo t = new Todo();
        t.setTitle("Title");
        Todo saved = todoRepository.save(t);

        assertNotNull(saved.getId());
        assertEquals(t.getTitle(), saved.getTitle());
    }

}
