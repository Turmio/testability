package fi.soisenniemi.testability;  
  
import javax.persistence.Id;  
import javax.persistence.GeneratedValue;  
import javax.persistence.Entity;  
  
@Entity
public class Todo {

  @Id @GeneratedValue  
  private Long id;  

  private String title;

  private Boolean completed = false;

  
  public Long getId() {
      return id;
  }

  public String getTitle() {
      return title;
  }

  public void setTitle(String value) {
      this.title = value;
  }

  public Boolean isCompleted() {
      return completed;
  }

  public void setCompleted(Boolean value) {
      this.completed = value;
  }
}