import {createApp} from 'vue'
import App from './App'
import { createLogger, StringifyObjectsHook }  from 'vue-logger-plugin';


const loggerOptions = {
  enabled: true,
  level: 'debug',
  beforeHooks: [
    StringifyObjectsHook
  ]
};


createApp(App).use(createLogger(loggerOptions)).mount("#app")