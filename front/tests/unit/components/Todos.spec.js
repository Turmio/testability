import { mount } from '@vue/test-utils'
import Todos from '@/components/Todos.vue'

test('Loading text is showed', () => {
  const wrapper = mount(Todos)

  expect(wrapper.text()).toContain("Loading...")
})

test('After loading placeholder text is available', () => {
  const wrapper = mount(Todos, {
    data() {
      return {
        loading: false
      }
    }
  })

  expect(wrapper.find(".new-todo").attributes('placeholder')).toBe('What needs to be done?')
})